<div class="box ">
    
    <div class="box-header">
        <h3 class="box-title">Matriculas</h3>
    </div>

    <?php 

        if(isset($actionForm)){

          $actionForm = base_url($this->router->class) . "/" . $actionForm;

        } else {
      
          $actionForm = base_url($this->router->class);
     
        }

        echo form_open( $actionForm, array('id' => 'form'));

        echo form_hidden('id', set_value('id', $matriculas['id']));
    ?>
    
    <form action="<?php echo $actionForm; ?>" method="post">
        
        <div class="box-body">
           
            <div class="row">
<div class="col-md-12">
<div class="form-group">
<label>Turmas Id</label>
<?php $js="id='turmas_id' class='form-control' data-msg-required='Campo obrigatório' required='required'";
echo form_dropdown('turmas_id', $Turmas_list, $matriculas['turmas_id'], $js); ?>
</div>
</div>
</div>
<div class="row">
<div class="col-md-12">
<div class="form-group">
<label>Alunos</label>
<?php $js="id='alunos_id' class='form-control' data-msg-required='Campo obrigatório' required='required'";
echo form_dropdown('alunos_id', $Alunos_list, $matriculas['alunos_id'], $js); ?>
</div>
</div>
</div>
<div class="row">
<div class="col-md-12">
<div class="form-group">
<label>Data Matricula</label>
<?php  echo form_input(array('name' =>'data_matricula', 'value' => set_value('data_matricula', $matriculas['data_matricula']), 'class' => 'form-control componenteData', 'placeholder' => '', 'data-msg-required' => 'Campo obrigatório', ));  ?>
</div>
</div>
</div>


            <div class="form-group">
                <button type="submit" class="btn btn-primary">Cadastrar</button>
            </div>
        </div>

    </form>

</div>