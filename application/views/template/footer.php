	<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

    <script src="<?php echo base_url("includes/assets/js/bootstrap.min.js"); ?>"></script>
    
    <script type="text/javascript" src="<?php echo base_url("includes/assets/js/app.js"); ?>"></script>

    <script type="text/javascript" src="<?php echo base_url("includes/assets/js/jquery.maskedinput.min.js"); ?>"></script>

    <script type="text/javascript" src="<?php echo base_url("includes/assets/js/bootstrap-datepicker.js"); ?>"></script>

    <script type="text/javascript" src="<?php echo base_url("includes/assets/js/datepicker.pt-BR.js"); ?>"></script>

    <script type="text/javascript" src="<?php echo base_url("includes/assets/js/lib/tinymce/js/tinymce/tinymce.min.js"); ?>"></script>

    <script src="<?php echo base_url("includes/assets/js/bootstrap-datetimepicker.min.js"); ?>"></script>

    <script src="<?php echo base_url("includes/assets/js/jquery.maskMoney.min.js"); ?>"></script>

    <script src="<?php echo base_url("includes/assets/js/chosen.jquery/js/chosen.jquery.min.js"); ?>"></script>

    <script type="text/javascript" src="http://cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>

    <script type="text/javascript" src="http://cdn.datatables.net/tabletools/2.2.1/js/dataTables.tableTools.min.js"></script>

    <script type="text/javascript">

        $(document).ready(function() {

            var controller = "<?php echo $this->router->fetch_class(); ?>";

            $("#menu li").each(function(e) { 

                if ($(this).attr('id') == controller) {
                    $(this).addClass('active');
                } else {
                    if ($(this).hasClass('active')) {
                        $(this).removeClass('active');
                    }
                }
             });

            $(".componenteData").each(function(e) { 

                var date = $(this).val();

                if (date.length > 0) { 
                   
                    var dateSplit = date.split("-");
                    
                    var dateConvert = dateSplit[2] + '/' + dateSplit[1] + '/' + dateSplit[0];

                    $(this).val(dateConvert);
                }

            });
            
            $('.componenteData').datepicker({
                format: 'dd/mm/yyyy',
                language: 'pt-BR'
            });

            $(".componenteDataHora").each(function(e) {

                var date = $(this).val();

                if (date.length > 0) { 
                   
                    var dataHora = date.split(" ");

                    var dateSplit = dataHora[0].split("-");
                    
                    var dateConvert = dateSplit[2] + '/' + dateSplit[1] + '/' + dateSplit[0];

                    $(this).val(dateConvert + " " + dataHora[1].substr(0, 5));
                }

            });

            $(function() {
                $('.componenteDataHora').datetimepicker({
                    format: 'dd/mm/yyyy hh:ii',
                    autoclose: true
                });
            });

            $("form").submit(function(){
                
                $(".componenteData").each(function(e) { 

                    var date = $(this).val();

                    if (date.length > 0) { 
                       
                        var dateSplit = date.split("/");

                        var dateConvert = dateSplit[2] + '-' + dateSplit[1] + '-' + dateSplit[0];

                        $(this).val(dateConvert);
                    }
                });

                $(".componenteDataHora").each(function(e) {

                    var date = $(this).val();

                    if (date.length > 0) { 
                       
                        var dataHora = date.split(" ");

                        var dateSplit = dataHora[0].split("/");
                        
                        var dateConvert = dateSplit[2] + '-' + dateSplit[1] + '-' + dateSplit[0];

                        $(this).val(dateConvert + " " + dataHora[1] + ":00");
                    }

                });
            });

            tinymce.init({
                selector: "textarea.tinymce",
                plugins: [
                    "advlist autolink lists link image charmap print preview anchor",
                    "searchreplace visualblocks code fullscreen",
                    "insertdatetime media table contextmenu paste "
                ],
                plugin_preview_width : "900",
                toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
            });


            var table = $('#datatable').DataTable( {
                "scrollX": true,
                "dom": 'T<"clear">lfrtip',
                "tableTools": {
                    "sSwfPath": 'includes/assets/swf/copy_csv_xls_pdf.swf',
                    "aButtons": [
                        {
                            "sExtends":       "collection",
                            "sButtonText": "Exportar",
                            "aButtons":    [ "xls", "pdf" ]
                        }
                    ]
                },
                "oLanguage": {
                    "sEmptyTable": "Nenhum registro encontrado",
                    "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
                    "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
                    "sInfoFiltered": "(Filtrados de _MAX_ registros)",
                    "sInfoPostFix": "",
                    "sInfoThousands": ".",
                    "sLengthMenu": "_MENU_ resultados por página",
                    "sLoadingRecords": "Carregando...",
                    "sProcessing": "Processando...",
                    "sZeroRecords": "Nenhum registro encontrado",
                    "sSearch": "Pesquisar",
                    "oPaginate": {
                        "sNext": "Próximo",
                        "sPrevious": "Anterior",
                        "sFirst": "Primeiro",
                        "sLast": "Último"
                    },
                    "oAria": {
                        "sSortAscending": ": Ordenar colunas de forma ascendente",
                        "sSortDescending": ": Ordenar colunas de forma descendente"
                    }
                }
            } );

        } );

    </script>
        
</body>

</html>