<?php $this->load->view('template/header'); ?>

        <div class="wrapper row-offcanvas row-offcanvas-left">
            <aside class="left-side sidebar-offcanvas">
                <section class="sidebar">
                    <ul class="sidebar-menu">
                    
                        <li class="active" id="principal">
                             <a href="<?php echo base_url(); ?>">
                                 <i class="fa fa-folder"></i>
                                 <span>Principal</span>
                             </a>
                        </li>

                        <?php $this->load->view('template/menu'); ?>

                    </ul>
                </section>
            </aside>

            <aside class="right-side">
                
                <?php $this->load->view('template/mensagens'); ?>

                <section class="content">

                    <?php  $this->load->view($content); ?>

                </section>

            </aside>
        </div>

<?php $this->load->view('template/footer'); ?>
