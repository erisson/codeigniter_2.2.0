<?php $message =  $this->session->flashdata('message'); ?>
<?php $message_error =  $this->session->flashdata('message_error'); ?>

<?php if ($message) { ?>
    <div class="pad margin no-print">
        <div class="alert alert-success" style="margin-bottom: 0!important;">
        <button type="button" class="close" data-dismiss="alert">×</button>
            <i class="fa fa-info"></i>
            <b><?php echo $this->session->flashdata('message'); ?></b>
        </div>
    </div>
<?php } ?>

<?php if ($message_error) { ?>
    <div class="pad margin no-print">
        <div class="alert alert-danger" style="margin-bottom: 0!important;">
        <button type="button" class="close" data-dismiss="alert">×</button>
            <i class="fa fa-info"></i>
            <b><?php echo $this->session->flashdata('message_error'); ?></b>
        </div>
    </div>
<?php } ?>

<?php   
 
    $this->session->set_flashdata('message', FALSE);
     
    $this->session->set_flashdata('message_error', FALSE);

?>