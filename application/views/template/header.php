<!DOCTYPE html>
<html>

    <head>
    
        <meta charset="UTF-8">

        <title>Software</title>

        <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" type="text/css" />

        <link href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />

        <link rel="icon" type="image/x-icon" href="<?php echo base_url("includes/assets/img/favicon.png"); ?>"/>

        <link rel="stylesheet" href="<?php echo base_url("includes/assets/css/AdminLTE.css"); ?>">

        <link rel="stylesheet" href="http://cdn.datatables.net/1.10.7/css/jquery.dataTables.css">

        <link rel="stylesheet" href="http://cdn.datatables.net/tabletools/2.2.1/css/dataTables.tableTools.min.css">

        <style>
            .jumbotron{
                padding: 30px;
                background-color: #fff;
                margin-bottom:0;
            }

            #datatable{
                margin-bottom: 20px;
            }
        </style>

    </head>

    <body class="skin-blue">
        <header class="header">
            <a href="<?php echo base_url(); ?>" class="logo">
                Software
            </a>
            <nav class="navbar navbar-static-top" role="navigation">
                <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <div class="navbar-right">
                    <ul class="nav navbar-nav">
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="glyphicon glyphicon-user"></i>
                                <span><i class="caret"></i></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="user-footer">
                                    <div class="pull-right">
                                        <a href="<?php  echo base_url('auth/logout');  ?>" class="btn btn-default btn-flat">Sair</a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>