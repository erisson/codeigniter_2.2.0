<?php $message_error =  $this->session->flashdata('message_error'); ?>

<!DOCTYPE html>
<html class="bg-black">
    <head>

        <meta charset="UTF-8">

        <title>Software | Login</title>

        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>

        <link rel="stylesheet" href="<?php echo base_url("includes/assets/css/bootstrap.min.css"); ?>">

        <link rel="icon" type="image/x-icon" href="<?php echo base_url("includes/assets/img/favicon.png"); ?>"/>

        <link rel="stylesheet" href="<?php echo base_url("includes/assets/css/AdminLTE.css"); ?>">

        <link href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />

    </head>

    <body class="bg-black">

        <div class="form-box" id="login-box">
            
            <?php if ($message_error) { ?>
                <div class="pad margin no-print">
                    <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                        <i class="fa fa-info"></i>
                        <b><?php echo $this->session->flashdata('message_error'); ?></b>
                    </div>
                </div>
            <?php } ?>
            
            <div class="header">Acesso ao Software</div>
            
            <?php echo form_open('auth/login', array('class' => 'form-signin')); ?>
               
                <div class="body bg-gray">

                    <div class="form-group">
                        <input type="text" name="username" class="form-control" placeholder="Usuário" value="demo"/>        
                    </div>

                    <div class="form-group">
                        <input type="password" name="password" class="form-control" placeholder="Senha" value="demo"/>        
                    </div>

                </div>

                <div class="footer">
                    <button type="submit" class="btn bg-olive btn-block">Entrar</button>
                </div>

            <?php form_close(); ?>

        </div> 

        <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js" type="text/javascript"></script>

    </body>
</html>