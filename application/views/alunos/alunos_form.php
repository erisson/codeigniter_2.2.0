<div class="box ">
    
    <div class="box-header">
        <h3 class="box-title">Alunos</h3>
    </div>

    <?php 

        if(isset($actionForm)){

          $actionForm = base_url($this->router->class) . "/" . $actionForm;

        } else {
      
          $actionForm = base_url($this->router->class);
     
        }

        echo form_open( $actionForm, array('id' => 'form'));

        echo form_hidden('id', set_value('id', $alunos['id']));
    ?>
    
    <form action="<?php echo $actionForm; ?>" method="post">
        
        <div class="box-body">
           
            <div class="row">
<div class="col-md-12">
<div class="form-group">
<label>Cpf</label>
<?php  echo form_input(array('name' =>'cpf', 'value' => set_value('cpf', $alunos['cpf']), 'class' => 'form-control', 'placeholder' => '', 'data-msg-required' => 'Campo obrigatório', 'required' => 'required'));  ?>
</div>
</div>
</div>
<div class="row">
<div class="col-md-12">
<div class="form-group">
<label>Nome</label>
<?php  echo form_input(array('name' =>'nome', 'value' => set_value('nome', $alunos['nome']), 'class' => 'form-control', 'placeholder' => '', 'data-msg-required' => 'Campo obrigatório', 'required' => 'required'));  ?>
</div>
</div>
</div>
<div class="row">
<div class="col-md-12">
<div class="form-group">
<label>Email</label>
<?php  echo form_input(array('name' =>'email', 'value' => set_value('email', $alunos['email']), 'class' => 'form-control', 'placeholder' => '', 'data-msg-required' => 'Campo obrigatório', 'required' => 'required'));  ?>
</div>
</div>
</div>
<div class="row">
<div class="col-md-12">
<div class="form-group">
<label>Fone</label>
<?php  echo form_input(array('name' =>'fone', 'value' => set_value('fone', $alunos['fone']), 'class' => 'form-control', 'placeholder' => '', 'data-msg-required' => 'Campo obrigatório', 'required' => 'required'));  ?>
</div>
</div>
</div>
<div class="row">
<div class="col-md-12">
<div class="form-group">
<label>Data Nascimento</label>
<?php  echo form_input(array('name' =>'data_nascimento', 'value' => set_value('data_nascimento', $alunos['data_nascimento']), 'class' => 'form-control componenteData', 'placeholder' => '', 'data-msg-required' => 'Campo obrigatório', ));  ?>
</div>
</div>
</div>


            <div class="form-group">
                <button type="submit" class="btn btn-primary">Cadastrar</button>
            </div>
        </div>

    </form>

</div>