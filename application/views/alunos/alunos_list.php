<div class="box">
    <div class="box-header">
        <h3 class="box-title">Alunos</h3>
    </div>
    
    <div class="box-body table-responsive">

	    <table id="datatable" class="display table table-striped table-bordered" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th>Id</th>
					<th>Cpf</th>
					<th>Nome</th>
					<th>Email</th>
					<th>Fone</th>
					<th>Data Nascimento</th>
					<th>Editar</th>
					<th>Deletar</th>
				</tr>
			</thead>

			<tbody>
				<?php foreach ($alunos as $key => $value) { ?>
					<tr>
						<td><?php  echo $value['id'];  ?></td>
						<td><?php  echo $value['cpf'];  ?></td>
						<td><?php  echo $value['nome'];  ?></td>
						<td><?php  echo $value['email'];  ?></td>
						<td><?php  echo $value['fone'];  ?></td>
						<td><?php  echo $value['data_nascimento'];  ?></td>
						<td><a href="<?php echo base_url($this->router->class . "/editar/" . $value['id']); ?>"><span class="glyphicon glyphicon-edit"></span></a></td>
						<td><a href="<?php echo base_url($this->router->class . "/deletar/" . $value['id']); ?>"><span class='glyphicon glyphicon-remove'></span></a></td>
					</tr>
				<?php } ?>
			</tbody>

		</table>
	    
	    <div class="form-group">
	        <button type="bottom" onclick="window.location.href='<?php echo base_url($this->router->class) . "/cadastrar"; ?>'" class="btn btn-primary">Cadastrar</button>
	    </div>

    </div>

</div>