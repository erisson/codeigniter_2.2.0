<div class="box">
    <div class="box-header">
        <h3 class="box-title">Users</h3>
    </div>
    
    <div class="box-body table-responsive">

	    <table id="datatable" class="display table table-striped table-bordered" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th>Id</th>
<th>Username</th>
<th>Password</th>
<th>Role</th>
<th>Status</th>
<th>Created</th>
<th>Modified</th>

					<th>Editar</th>
					<th>Deletar</th>
				</tr>
			</thead>

			<tbody>
				<?php foreach ($users as $key => $value) { ?>
					<tr>
						<td><?php  echo $value['id'];  ?></td>
<td><?php  echo $value['username'];  ?></td>
<td><?php  echo $value['password'];  ?></td>
<td><?php  echo $value['role'];  ?></td>
<td><?php  echo $value['status'];  ?></td>
<td><?php  echo $value['created'];  ?></td>
<td><?php  echo $value['modified'];  ?></td>

						<td><a href="<?php echo base_url($this->router->class . "/editar/" . $value['id']); ?>"><span class="glyphicon glyphicon-edit"></span></a></td>
						<td><a href="<?php echo base_url($this->router->class . "/deletar/" . $value['id']); ?>"><span class='glyphicon glyphicon-remove'></span></a></td>
					</tr>
				<?php } ?>
			</tbody>

		</table>
	    
	    <div class="form-group">
	        <button type="bottom" onclick="window.location.href='<?php echo base_url($this->router->class) . "/cadastrar"; ?>'" class="btn btn-primary">Cadastrar</button>
	    </div>

    </div>

</div>