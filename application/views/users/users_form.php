<div class="box ">
    
    <div class="box-header">
        <h3 class="box-title">Users</h3>
    </div>

    <?php 

        if(isset($actionForm)){

          $actionForm = base_url($this->router->class) . "/" . $actionForm;

        } else {
      
          $actionForm = base_url($this->router->class);
     
        }

        echo form_open( $actionForm, array('id' => 'form'));

        echo form_hidden('id', set_value('id', $users['id']));
    ?>
    
    <form action="<?php echo $actionForm; ?>" method="post">
        
        <div class="box-body">
           
            <div class="row">
<div class="col-md-12">
<div class="form-group">
<label>Username</label>
<?php  echo form_input(array('name' =>'username', 'value' => set_value('username', $users['username']), 'class' => 'form-control', 'placeholder' => '', 'data-msg-required' => 'Campo obrigatório', 'required' => 'required'));  ?>
</div>
</div>
</div>
<div class="row">
<div class="col-md-12">
<div class="form-group">
<label>Password</label>
<?php  echo form_input(array('name' =>'password', 'value' => set_value('password', $users['password']), 'class' => 'form-control', 'placeholder' => '', 'data-msg-required' => 'Campo obrigatório', 'required' => 'required'));  ?>
</div>
</div>
</div>
<div class="row">
<div class="col-md-12">
<div class="form-group">
<label>Role</label>
<?php  echo form_input(array('name' =>'role', 'value' => set_value('role', $users['role']), 'class' => 'form-control', 'placeholder' => '', 'data-msg-required' => 'Campo obrigatório', ));  ?>
</div>
</div>
</div>
<div class="row">
<div class="col-md-12">
<div class="form-group">
<label>Status</label>
<?php  echo form_input(array('name' =>'status', 'value' => set_value('status', $users['status']), 'class' => 'form-control', 'placeholder' => '', 'data-msg-required' => 'Campo obrigatório', ));  ?>
</div>
</div>
</div>
<div class="row">
<div class="col-md-12">
<div class="form-group">
<label>Created</label>
<?php  echo form_input(array('name' =>'created', 'value' => set_value('created', $users['created']), 'class' => 'form-control', 'placeholder' => '', 'data-msg-required' => 'Campo obrigatório', ));  ?>
</div>
</div>
</div>
<div class="row">
<div class="col-md-12">
<div class="form-group">
<label>Modified</label>
<?php  echo form_input(array('name' =>'modified', 'value' => set_value('modified', $users['modified']), 'class' => 'form-control', 'placeholder' => '', 'data-msg-required' => 'Campo obrigatório', ));  ?>
</div>
</div>
</div>


            <div class="form-group">
                <button type="submit" class="btn btn-primary">Cadastrar</button>
            </div>
        </div>

    </form>

</div>