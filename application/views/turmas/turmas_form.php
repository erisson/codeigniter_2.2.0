<div class="box ">
    
    <div class="box-header">
        <h3 class="box-title">Turmas</h3>
    </div>

    <?php 

        if(isset($actionForm)){

          $actionForm = base_url($this->router->class) . "/" . $actionForm;

        } else {
      
          $actionForm = base_url($this->router->class);
     
        }

        echo form_open( $actionForm, array('id' => 'form'));

        echo form_hidden('id', set_value('id', $turmas['id']));
    ?>
    
    <form action="<?php echo $actionForm; ?>" method="post">
        
        <div class="box-body">
           
            <div class="row">
<div class="col-md-12">
<div class="form-group">
<label>Instrutores</label>
<?php $js="id='instrutores_id' class='form-control' data-msg-required='Campo obrigatório' required='required'";
echo form_dropdown('instrutores_id', $Instrutores_list, $turmas['instrutores_id'], $js); ?>
</div>
</div>
</div>
<div class="row">
<div class="col-md-12">
<div class="form-group">
<label>Cursos</label>
<?php $js="id='cursos_id' class='form-control' data-msg-required='Campo obrigatório' required='required'";
echo form_dropdown('cursos_id', $Cursos_list, $turmas['cursos_id'], $js); ?>
</div>
</div>
</div>
<div class="row">
<div class="col-md-12">
<div class="form-group">
<label>Data Inicio</label>
<?php  echo form_input(array('name' =>'data_inicio', 'value' => set_value('data_inicio', $turmas['data_inicio']), 'class' => 'form-control componenteData', 'placeholder' => '', 'data-msg-required' => 'Campo obrigatório', ));  ?>
</div>
</div>
</div>
<div class="row">
<div class="col-md-12">
<div class="form-group">
<label>Data Final</label>
<?php  echo form_input(array('name' =>'data_final', 'value' => set_value('data_final', $turmas['data_final']), 'class' => 'form-control componenteData', 'placeholder' => '', 'data-msg-required' => 'Campo obrigatório', ));  ?>
</div>
</div>
</div>


            <div class="form-group">
                <button type="submit" class="btn btn-primary">Cadastrar</button>
            </div>
        </div>

    </form>

</div>