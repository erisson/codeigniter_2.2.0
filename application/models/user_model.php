<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_Model extends CI_Model {

	function __construct()
    {      
    	parent::__construct();	
    }

    public function validate() {

        $this->db->where('username', $this->input->post('username')); 
        $this->db->where('password', $this->input->post('password'));
        $this->db->where('status', 1);

        $query = $this->db->get('users'); 

        if ($query->num_rows == 1) { 
            return true;
        }
    }
}
