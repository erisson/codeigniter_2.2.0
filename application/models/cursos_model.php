<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cursos_Model extends CI_Model {

	function __construct()
    {      
    	parent::__construct();	
    }

	public function listarTodos()
    {        
        $query = $this->db->get('cursos');
        
        return $query->result_array();
    }

    public function listaCombo()
    {
        $this->db->order_by('id', 'ASC');
    
        $query  = $this->db->get('cursos');
        
        $return = array();
        $return[""] = "";
        foreach($query->result() as $option)
        {
            $return[$option->id] = $option->nome;
        }
        return $return; 
    }

    public function listarByCodigo($id)
    {        
        $this->db->where('id', $id);

        $query = $this->db->get('cursos');

        return $query->row_array();
    }

    public function cadastrar($array = array())
    {       
        $this->db->insert('cursos', $array);

        return $this->db->insert_id();
    }

    public function atualizar($array = array())
    {        
        $this->db->where('id', $array['id']);

        $data = $this->db->update('cursos', $array);

        if ($data) {
            return true;
        }
        return false;
    }

    public function deletar($id)
    {        
        $this->db->where('id', $id);

        $data = $this->db->delete('cursos');

        if ($data) {
            return true;
        }
        return false;
    }
}
