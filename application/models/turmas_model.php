<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Turmas_Model extends CI_Model {

	function __construct()
    {      
    	parent::__construct();	
    }

	public function listarTodos()
    {        
        $query = $this->db->get('turmas');
        
        return $query->result_array();
    }

    public function listarTodosComInstrutorECurso()
    {
        $this->db->select('turmas.*, instrutores.nome as nomeInstrutor, cursos.nome as nomeCurso');
        $this->db->from('turmas');
        $this->db->join('instrutores', 'instrutores.id = turmas.instrutores_id');
        $this->db->join('cursos', 'cursos.id = turmas.cursos_id');
        $query = $this->db->get();

        return $query->result_array();
    }

    public function listaCombo()
    {
        $this->db->order_by('id', 'ASC');
    
        $query  = $this->db->get('turmas');
        
        $return = array();
        $return[""] = "";
        foreach($query->result() as $option)
        {
            $return[$option->id] = $option->id;
        }
        return $return;
    }

    public function listarByCodigo($id)
    {        
        $this->db->where('id', $id);

        $query = $this->db->get('turmas');

        return $query->row_array();
    }

    public function cadastrar($array = array())
    {       
        $this->db->insert('turmas', $array);

        return $this->db->insert_id();
    }

    public function atualizar($array = array())
    {        
        $this->db->where('id', $array['id']);

        $data = $this->db->update('turmas', $array);

        if ($data) {
            return true;
        }
        return false;
    }

    public function deletar($id)
    {        
        $this->db->where('id', $id);

        $data = $this->db->delete('turmas');

        if ($data) {
            return true;
        }
        return false;
    }
}
