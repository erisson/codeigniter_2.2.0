<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Alunos_Model extends CI_Model {

	function __construct()
    {      
    	parent::__construct();	
    }

	public function listarTodos()
    {        
        $query = $this->db->get('alunos');
        
        return $query->result_array();
    }

    public function listaCombo()
    {
        $this->db->order_by('id', 'ASC');
    
        $query  = $this->db->get('alunos');
        
        $return = array();
        $return[""] = "";
        foreach($query->result() as $option)
        {
            //$return[$option->id] = $option->id;  //ira mostrar apenas o id no combobox
            $return[$option->id] = $option->nome;  //mostra o nome mas a posicao do array sera o proprio id do aluno
        }
        return $return; 
    }

    public function listarByCodigo($id)
    {        
        $this->db->where('id', $id);

        $query = $this->db->get('alunos');

        return $query->row_array();
    }

    public function cadastrar($array = array())
    {       
        $this->db->insert('alunos', $array);

        return $this->db->insert_id();
    }

    public function atualizar($array = array())
    {        
        $this->db->where('id', $array['id']);

        $data = $this->db->update('alunos', $array);

        if ($data) {
            return true;
        }
        return false;
    }

    public function deletar($id)
    {        
        $this->db->where('id', $id);

        $data = $this->db->delete('alunos');

        if ($data) {
            return true;
        }
        return false;
    }
}
