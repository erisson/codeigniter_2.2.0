<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users extends MY_Controller {

	function __construct()
    {      
    	parent::__construct();	

        $this->uri_base = 'users';

        $this->load->model('Users_Model');

        
    }

	public function index()
	{   
        $data['users'] = $this->Users_Model->listarTodos();

		$data['content'] = 'users/users_list';
        $this->load->view('template/main', $data);
	}

    public function cadastrar()
    {   
        if ($this->input->post()) 
        {   
            $dataForm = $this->inicializaDados();

            $dataForm = array_filter($dataForm);

            $data = $this->Users_Model->cadastrar($dataForm);

            if ($data) {
                $this->session->set_flashdata ('message', 'Users cadastrado com sucesso!');
            } else {
                $this->session->set_flashdata ('message_error', 'Erro ao cadastrar users!');
            }

            redirect('users');
        }

        $data = $this->obterListaCombos();
        $data['users'] = $this->inicializaDados();

        $data['actionForm'] = 'cadastrar';
        $data['content'] = 'users/users_form';
        $this->load->view('template/main', $data);
    }

    public function editar($id = null)
    {   
        if ($this->input->post()) 
        {   
            $dataForm = $this->inicializaDados();

            $dataForm = array_filter($dataForm);

            $data = $this->Users_Model->atualizar($dataForm);
            
            if ($data) {
                $this->session->set_flashdata ('message', 'Users atualizado com sucesso!');
            } else {
                $this->session->set_flashdata ('message_error', 'Erro ao atualizar users!');
            }

            redirect('users');

        } else {
            $data = $this->obterListaCombos();
            $data['users'] = $this->Users_Model->listarByCodigo($id);
        }

        $data['actionForm'] = 'editar';
        $data['content'] = 'users/users_form';
        $this->load->view('template/main', $data);
    }

    public function deletar($id)
    {
        $data = $this->Users_Model->deletar($id);

        if ($data) {
            $this->session->set_flashdata ('message', 'Users excluído com sucesso!');
        } else {
            $this->session->set_flashdata ('message_error', 'Erro ao excluir users!');
        }

        redirect('users');
    }

    private function inicializaDados()
    {
        $users = array();

        $users['id'] = $this->input->post('id');
$users['username'] = $this->input->post('username');
$users['password'] = $this->input->post('password');
$users['role'] = $this->input->post('role');
$users['status'] = $this->input->post('status');
$users['created'] = $this->input->post('created');
$users['modified'] = $this->input->post('modified');

        return $users;
    }

    private function obterListaCombos() 
    {
        $users = array();

        
        return $users;
    }

}
