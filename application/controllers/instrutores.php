<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Instrutores extends MY_Controller {

	function __construct()
    {      
    	parent::__construct();	

        $this->uri_base = 'instrutores';

        $this->load->model('Instrutores_Model');

        
    }

	public function index()
	{   
        $data['instrutores'] = $this->Instrutores_Model->listarTodos();

		$data['content'] = 'instrutores/instrutores_list';
        $this->load->view('template/main', $data);
	}

    public function cadastrar()
    {   
        if ($this->input->post()) 
        {   
            $dataForm = $this->inicializaDados();

            $dataForm = array_filter($dataForm);

            $data = $this->Instrutores_Model->cadastrar($dataForm);

            if ($data) {
                $this->session->set_flashdata ('message', 'Instrutores cadastrado com sucesso!');
            } else {
                $this->session->set_flashdata ('message_error', 'Erro ao cadastrar instrutores!');
            }

            redirect('instrutores');
        }

        $data = $this->obterListaCombos();
        $data['instrutores'] = $this->inicializaDados();

        $data['actionForm'] = 'cadastrar';
        $data['content'] = 'instrutores/instrutores_form';
        $this->load->view('template/main', $data);
    }

    public function editar($id = null)
    {   
        if ($this->input->post()) 
        {   
            $dataForm = $this->inicializaDados();

            $dataForm = array_filter($dataForm);

            $data = $this->Instrutores_Model->atualizar($dataForm);
            
            if ($data) {
                $this->session->set_flashdata ('message', 'Instrutores atualizado com sucesso!');
            } else {
                $this->session->set_flashdata ('message_error', 'Erro ao atualizar instrutores!');
            }

            redirect('instrutores');

        } else {
            $data = $this->obterListaCombos();
            $data['instrutores'] = $this->Instrutores_Model->listarByCodigo($id);
        }

        $data['actionForm'] = 'editar';
        $data['content'] = 'instrutores/instrutores_form';
        $this->load->view('template/main', $data);
    }

    public function deletar($id)
    {
        $data = $this->Instrutores_Model->deletar($id);

        if ($data) {
            $this->session->set_flashdata ('message', 'Instrutores excluído com sucesso!');
        } else {
            $this->session->set_flashdata ('message_error', 'Erro ao excluir instrutores!');
        }

        redirect('instrutores');
    }

    private function inicializaDados()
    {
        $instrutores = array();

        $instrutores['id'] = $this->input->post('id');
$instrutores['nome'] = $this->input->post('nome');
$instrutores['email'] = $this->input->post('email');

        return $instrutores;
    }

    private function obterListaCombos() 
    {
        $instrutores = array();

        
        return $instrutores;
    }

}
