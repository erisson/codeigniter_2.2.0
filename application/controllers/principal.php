<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Principal extends MY_Controller {

	function __construct()
    {      
    	parent::__construct();	

        $this->uri_base = "principal";
    }

	public function index()
	{   
		$data['content'] = 'principal';

        $this->load->view('template/main', $data);
	}

}
