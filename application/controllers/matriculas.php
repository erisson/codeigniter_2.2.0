<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Matriculas extends MY_Controller {

	function __construct()
    {      
    	parent::__construct();	

        $this->uri_base = 'matriculas';

        $this->load->model('Matriculas_Model');

        $this->load->model('Turmas_Model');

        $this->load->model('Alunos_Model');
    }

	public function index()
	{   
        $data['matriculas'] = $this->Matriculas_Model->listarTodosComAluno();

		$data['content'] = 'matriculas/matriculas_list';
        $this->load->view('template/main', $data);
	}

    public function cadastrar()
    {   
        if ($this->input->post()) 
        {   
            $dataForm = $this->inicializaDados();

            $dataForm = array_filter($dataForm);

            $data = $this->Matriculas_Model->cadastrar($dataForm);

            if ($data) {
                $this->session->set_flashdata ('message', 'Matriculas cadastrado com sucesso!');
            } else {
                $this->session->set_flashdata ('message_error', 'Erro ao cadastrar matriculas!');
            }

            redirect('matriculas');
        }

        $data = $this->obterListaCombos();
        $data['matriculas'] = $this->inicializaDados();

        $data['actionForm'] = 'cadastrar';
        $data['content'] = 'matriculas/matriculas_form';
        $this->load->view('template/main', $data);
    }

    public function editar($id = null)
    {   
        if ($this->input->post()) 
        {   
            $dataForm = $this->inicializaDados();

            $dataForm = array_filter($dataForm);

            $data = $this->Matriculas_Model->atualizar($dataForm);
            
            if ($data) {
                $this->session->set_flashdata ('message', 'Matriculas atualizado com sucesso!');
            } else {
                $this->session->set_flashdata ('message_error', 'Erro ao atualizar matriculas!');
            }

            redirect('matriculas');

        } else {
            $data = $this->obterListaCombos();
            $data['matriculas'] = $this->Matriculas_Model->listarByCodigo($id);
        }

        $data['actionForm'] = 'editar';
        $data['content'] = 'matriculas/matriculas_form';
        $this->load->view('template/main', $data);
    }

    public function deletar($id)
    {
        $data = $this->Matriculas_Model->deletar($id);

        if ($data) {
            $this->session->set_flashdata ('message', 'Matriculas excluído com sucesso!');
        } else {
            $this->session->set_flashdata ('message_error', 'Erro ao excluir matriculas!');
        }

        redirect('matriculas');
    }

    private function inicializaDados()
    {
        $matriculas = array();

        $matriculas['id'] = $this->input->post('id');
        $matriculas['turmas_id'] = $this->input->post('turmas_id');
        $matriculas['alunos_id'] = $this->input->post('alunos_id');
        $matriculas['data_matricula'] = $this->input->post('data_matricula');

        return $matriculas;
    }

    private function obterListaCombos() 
    {
        $matriculas = array();

        $matriculas['Turmas_list'] = $this->Turmas_Model->listaCombo();
        $matriculas['Alunos_list'] = $this->Alunos_Model->listaCombo();

        /*$matriculas['Alunos_list'] = array(
                                            '1'        => 'Erisson',
                                            '2'        => 'Maria',
                                            '3'        => 'Joao',
                                            '4'        => 'Alice',
                                        );*/
        return $matriculas;
    }

}
