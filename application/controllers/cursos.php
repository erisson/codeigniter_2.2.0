<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cursos extends MY_Controller {

	function __construct()
    {      
    	parent::__construct();	

        $this->uri_base = 'cursos';

        $this->load->model('Cursos_Model');

        
    }

	public function index()
	{   
        $data['cursos'] = $this->Cursos_Model->listarTodos();

		$data['content'] = 'cursos/cursos_list';
        $this->load->view('template/main', $data);
	}

    public function cadastrar()
    {   
        if ($this->input->post()) 
        {   
            $dataForm = $this->inicializaDados();

            $dataForm = array_filter($dataForm);

            $data = $this->Cursos_Model->cadastrar($dataForm);

            if ($data) {
                $this->session->set_flashdata ('message', 'Cursos cadastrado com sucesso!');
            } else {
                $this->session->set_flashdata ('message_error', 'Erro ao cadastrar cursos!');
            }

            redirect('cursos');
        }

        $data = $this->obterListaCombos();
        $data['cursos'] = $this->inicializaDados();

        $data['actionForm'] = 'cadastrar';
        $data['content'] = 'cursos/cursos_form';
        $this->load->view('template/main', $data);
    }

    public function editar($id = null)
    {   
        if ($this->input->post()) 
        {   
            $dataForm = $this->inicializaDados();

            $dataForm = array_filter($dataForm);

            $data = $this->Cursos_Model->atualizar($dataForm);
            
            if ($data) {
                $this->session->set_flashdata ('message', 'Cursos atualizado com sucesso!');
            } else {
                $this->session->set_flashdata ('message_error', 'Erro ao atualizar cursos!');
            }

            redirect('cursos');

        } else {
            $data = $this->obterListaCombos();
            $data['cursos'] = $this->Cursos_Model->listarByCodigo($id);
        }

        $data['actionForm'] = 'editar';
        $data['content'] = 'cursos/cursos_form';
        $this->load->view('template/main', $data);
    }

    public function deletar($id)
    {
        $data = $this->Cursos_Model->deletar($id);

        if ($data) {
            $this->session->set_flashdata ('message', 'Cursos excluído com sucesso!');
        } else {
            $this->session->set_flashdata ('message_error', 'Erro ao excluir cursos!');
        }

        redirect('cursos');
    }

    private function inicializaDados()
    {
        $cursos = array();

        $cursos['id'] = $this->input->post('id');
$cursos['nome'] = $this->input->post('nome');
$cursos['carga_horaria'] = $this->input->post('carga_horaria');

        return $cursos;
    }

    private function obterListaCombos() 
    {
        $cursos = array();

        
        return $cursos;
    }

}
