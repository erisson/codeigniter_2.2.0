<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Turmas extends MY_Controller {

	function __construct()
    {      
    	parent::__construct();	

        $this->uri_base = 'turmas';

        $this->load->model('Turmas_Model');

        $this->load->model('Instrutores_Model');$this->load->model('Cursos_Model');
    }

	public function index()
	{   
        $data['turmas'] = $this->Turmas_Model->listarTodosComInstrutorECurso();

		$data['content'] = 'turmas/turmas_list';
        $this->load->view('template/main', $data);
	}

    public function cadastrar()
    {   
        if ($this->input->post()) 
        {   
            $dataForm = $this->inicializaDados();

            $dataForm = array_filter($dataForm);

            $data = $this->Turmas_Model->cadastrar($dataForm);

            if ($data) {
                $this->session->set_flashdata ('message', 'Turmas cadastrado com sucesso!');
            } else {
                $this->session->set_flashdata ('message_error', 'Erro ao cadastrar turmas!');
            }

            redirect('turmas');
        }

        $data = $this->obterListaCombos();
        $data['turmas'] = $this->inicializaDados();

        $data['actionForm'] = 'cadastrar';
        $data['content'] = 'turmas/turmas_form';
        $this->load->view('template/main', $data);
    }

    public function editar($id = null)
    {   
        if ($this->input->post()) 
        {   
            $dataForm = $this->inicializaDados();

            $dataForm = array_filter($dataForm);

            $data = $this->Turmas_Model->atualizar($dataForm);
            
            if ($data) {
                $this->session->set_flashdata ('message', 'Turmas atualizado com sucesso!');
            } else {
                $this->session->set_flashdata ('message_error', 'Erro ao atualizar turmas!');
            }

            redirect('turmas');

        } else {
            $data = $this->obterListaCombos();
            $data['turmas'] = $this->Turmas_Model->listarByCodigo($id);
        }

        $data['actionForm'] = 'editar';
        $data['content'] = 'turmas/turmas_form';
        $this->load->view('template/main', $data);
    }

    public function deletar($id)
    {
        $data = $this->Turmas_Model->deletar($id);

        if ($data) {
            $this->session->set_flashdata ('message', 'Turmas excluído com sucesso!');
        } else {
            $this->session->set_flashdata ('message_error', 'Erro ao excluir turmas!');
        }

        redirect('turmas');
    }

    private function inicializaDados()
    {
        $turmas = array();

        $turmas['id'] = $this->input->post('id');
        $turmas['instrutores_id'] = $this->input->post('instrutores_id');
        $turmas['cursos_id'] = $this->input->post('cursos_id');
        $turmas['data_inicio'] = $this->input->post('data_inicio');
        $turmas['data_final'] = $this->input->post('data_final');

        return $turmas;
    }

    private function obterListaCombos() 
    {
        $turmas = array();

        $turmas['Instrutores_list'] = $this->Instrutores_Model->listaCombo();
        $turmas['Cursos_list'] = $this->Cursos_Model->listaCombo();

        return $turmas;
    }

}
