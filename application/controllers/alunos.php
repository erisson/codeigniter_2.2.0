<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Alunos extends MY_Controller {

	function __construct()
    {      
    	parent::__construct();	

        $this->uri_base = 'alunos';

        $this->load->model('Alunos_Model');

    }

	public function index()
	{   
        $data['alunos'] = $this->Alunos_Model->listarTodos();

		$data['content'] = 'alunos/alunos_list';
        $this->load->view('template/main', $data);
	}

    public function cadastrar()
    {   
        if ($this->input->post()) 
        {   
            $dataForm = $this->inicializaDados();

            $dataForm = array_filter($dataForm);

            $data = $this->Alunos_Model->cadastrar($dataForm);

            if ($data) {
                $this->session->set_flashdata ('message', 'Alunos cadastrado com sucesso!');
            } else {
                $this->session->set_flashdata ('message_error', 'Erro ao cadastrar alunos!');
            }

            redirect('alunos');
        }

        $data = $this->obterListaCombos();
        $data['alunos'] = $this->inicializaDados();

        $data['actionForm'] = 'cadastrar';
        $data['content'] = 'alunos/alunos_form';
        $this->load->view('template/main', $data);
    }

    public function editar($id = null)
    {   
        if ($this->input->post()) 
        {   
            $dataForm = $this->inicializaDados();

            $dataForm = array_filter($dataForm);

            $data = $this->Alunos_Model->atualizar($dataForm);
            
            if ($data) {
                $this->session->set_flashdata ('message', 'Alunos atualizado com sucesso!');
            } else {
                $this->session->set_flashdata ('message_error', 'Erro ao atualizar alunos!');
            }

            redirect('alunos');

        } else {
            $data = $this->obterListaCombos();
            $data['alunos'] = $this->Alunos_Model->listarByCodigo($id);
        }

        $data['actionForm'] = 'editar';
        $data['content'] = 'alunos/alunos_form';
        $this->load->view('template/main', $data);
    }

    public function deletar($id)
    {
        $data = $this->Alunos_Model->deletar($id);

        if ($data) {
            $this->session->set_flashdata ('message', 'Alunos excluído com sucesso!');
        } else {
            $this->session->set_flashdata ('message_error', 'Erro ao excluir alunos!');
        }

        redirect('alunos');
    }

    private function inicializaDados()
    {
        $alunos = array();

        $alunos['id'] = $this->input->post('id');
        $alunos['cpf'] = $this->input->post('cpf');
        $alunos['nome'] = $this->input->post('nome');
        $alunos['email'] = $this->input->post('email');
        $alunos['fone'] = $this->input->post('fone');
        $alunos['data_nascimento'] = $this->input->post('data_nascimento');

        return $alunos;
    }

    private function obterListaCombos() 
    {
        $alunos = array();

        
        return $alunos;
    }

}
