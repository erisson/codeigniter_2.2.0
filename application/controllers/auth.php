<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth extends CI_Controller {

	function __construct()
    {      
    	parent::__construct();	

        $this->uri_base = "auth";

        $this->load->model('User_Model');
    }

	public function index()
	{   
		$data['content'] = 'principal';

        $this->load->view('template/main', $data);
	}

	function login() {

        $this->load->library('form_validation');

        $this->form_validation->set_rules('username', 'Username', 'required');
        
        $this->form_validation->set_rules('password', 'Password', 'required');

        $this->form_validation->set_error_delimiters('<p class="error">', '</p>');

        $query = $this->User_Model->validate();

        if ($this->form_validation->run() == FALSE) {

            redirect('login', 'refresh');

        } else {

            if ($query) { 

                $data = array(
                    'username' => $this->input->post('username'),
                    'logged' => true
                );

                $this->session->set_userdata($data);

                $this->session->set_flashdata('message_error', FALSE);

                redirect('principal');

            } else {

                $this->session->set_flashdata ('message_error', 'Erro ao acessar, por favor tente novamente!');
                
                redirect('login', 'refresh');

            }
        }
    }

	public function logout()
    {
        $this->session->unset_userdata('logged');
        
        redirect('login', 'refresh');
    }

}
